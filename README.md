# Kaggle_Earthquake_

This repo refers to the LAN Earthquake KAGGLE Competition.
The main goal is applying some kind of machine learning techniques in order to predict the time remaining until the following earthquake event.
The entire dataset is based on AE (acoustic emissions) measured through an accelerometer, in a laboratory environment, simulating the fracture event through a fault gouge material submitted to a double direct shear.

### Dataset

    - Training set
         - Time contiguous acoustic signal 
    - Test set
         - Small chunks of time contiguous acoustic signal
                
### Expected output
    - A series of TTF(time to failure) predictions (measured in   seconds) for each test set chunk


The basic idea is retrieving statistical information out of each single chunk, figure out some feature selection mechanism and apply the inferred relations for the training process.

    
    
    -Random Forest
    -LSTM

